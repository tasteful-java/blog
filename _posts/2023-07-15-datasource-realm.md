---
layout: post
author: Maurice
title: An Alternative to Tomcat's DataSourceRealm
published: false
---

# An Alternative to Tomcat's DataSourceRealm

Tomcat's DataSourceRealm (as well as the JDBCRealm) makes certains assumptions
about the schema of the database that make it less flexible than it could be.
That's why I ended up by writing my own security realm.



Source code here: [https://gitlab.com/_javalibs/mydsrealm](https://gitlab.com/_javalibs/mydsrealm)
