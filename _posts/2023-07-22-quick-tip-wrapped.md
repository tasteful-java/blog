---
layout: post
author: Maurice
title: Quick tip: Wrapping Exceptions
published: false
---

# Quick tip: Wrapping Exceptions



```java
public class WrappedException extends RuntimeException {

    public static WrappedException wrap(Throwable e) {
        return e instanceof WrappedException
                ? (WrappedException)e : new WrappedException(e);
    }

    private WrappedException(Throwable e) {
        super(e.getMessage(), e);
    }

    public <T extends Throwable> void rethrow(Class<T> clazz) throws T {
        for (Throwable e = getCause(); e != null; e = e.getCause()) {
            if (clazz.isInstance(e)) {
                throw clazz.cast(e);
            }
        }
    }

    @FunctionalInterface
    public static interface ThrowingRunnable {
        public void run() throws Throwable;
    }

    @FunctionalInterface
    public static interface ThrowingSupplier<T> {
        public T get() throws Throwable;
    }

    public static <T extends Throwable,R> R throwing(
            Supplier<R> code, Class<T> clazz) throws T {
        try {
            return code.get();
        } catch (WrappedException e) {
            e.rethrow(clazz);
            throw e;
        } catch (RuntimeException | Error e) {
            throw e;
        } catch (Throwable e) {
            if (clazz.isInstance(e)) {
                throw clazz.cast(e);
            }
            throw wrap(e);
        }
    }

    public static <T extends Throwable> void throwing(
            Runnable code, Class<T> clazz) throws T {
        throwing(() -> {
            code.run();
            return null;
        }, clazz);
    }

    public static <T extends Throwable, U extends Throwable, R> R throwing(
            Supplier<R> code, Class<T> c1, Class<U> c2) throws T, U {
        try {
            return code.get();
        } catch (WrappedException e) {
            e.rethrow(c1);
            e.rethrow(c2);
            throw e;
        } catch (RuntimeException | Error e) {
            throw e;
        } catch (Throwable e) {
            if (c1.isInstance(e)) {
                throw c1.cast(e);
            } else if (c2.isInstance(e)) {
                throw c2.cast(e);
            }
            throw wrap(e);
        }
    }

    public static <T extends Throwable, U extends Throwable> void throwing(
            Runnable code, Class<T> c1, Class<U> c2) throws T, U {
        throwing(() -> {
            code.run();
            return null;
        }, c1, c2);
    }

    public static <T> T wrapping(ThrowingSupplier<T> code) {
        try {
            return code.get();
        } catch (RuntimeException | Error ex) {
            throw ex;
        } catch (Throwable ex) {
            throw wrap(ex);
        }
    }

    public static void wrapping(ThrowingRunnable code) {
        wrapping(() -> {
            code.run();
            return null;
        });
    }
}
```

Source code here: [https://gitlab.com/_java-examples/wrappedexception](https://gitlab.com/_java-examples/wrappedexception)
