---
layout: main
title: Tasteful Java
---
# Tasteful Java

Java doesn't have to be complicated; it has to be tasteful though
